/*
 * Player.cpp
 *
 *  Created on: Jun 29, 2016
 *      Author: jschilling
 */

#include "Player.h"

#include <vector>

using namespace std;

Player::Player(int nrOfPlayers) {
	  // Set up sizes. (HEIGHT x WIDTH)
	  ergs.resize(nrOfPlayers);
	  for (int i = 0; i < nrOfPlayers; ++i)
	    ergs[i].resize(2);
}

Player::~Player() {

}

