/*
 * People.h
 *
 *  Created on: Jun 28, 2016
 *      Author: jschilling
 */

#ifndef PEOPLE_H_
#define PEOPLE_H_

#include <string>

#include "Mammal.h"

class People : public Mammal {
// Constructor and Destructor
public:
	People(std::string name, unsigned int age);
	virtual ~People();

	virtual void scream();

// Member Variables
private:
	std::string mName;
	int mAge;

// Getters and Setters
public:
	int getAge() const {
		return mAge;
	}

	void setAge(int age) {
		mAge = age;
	}

	const std::string& getName() const {
		return mName;
	}

	void setName(const std::string& name) {
		mName = name;
	}

};

#endif /* PEOPLE_H_ */
