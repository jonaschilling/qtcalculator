/*
 * Player.h
 *
 *  Created on: Jun 29, 2016
 *      Author: jschilling
 */

#ifndef PLAYER_H_
#define PLAYER_H_


#include <vector>


using namespace std;


class Player {

public:
	Player(int nrOfPlayers);
	virtual ~Player();

private:
	vector<vector<int>> ergs;


public:
	const vector<int> getErg(int nr) const {
		vector<int> erg;
		erg.resize(2);
		erg[0] = ergs[nr][0];
		erg[1] = ergs[nr][1];
		return erg;
	}

	void setErg(int nr, int won, int lost){
		ergs[nr][0] = won;
		ergs[nr][1] = lost;
	}


};

#endif /* PLAYER_H_ */
