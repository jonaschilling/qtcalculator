/*
 * Student.cpp
 *
 *  Created on: Jun 28, 2016
 *      Author: jschilling
 */

#include "Student.h"

#include <iostream>

using namespace std;

Student::Student(int matrikel, std::string name, int age) : People(name,age){
	this->mMatrikel = matrikel;
}

Student::~Student() {
	cout << "Student: " << this->getName() << " " <<
			this->mMatrikel << " is destroyed!" << endl;
}

void Student::scream() {
	People::scream();
	cout << "I am a MAMMAL and People and Student " << mMatrikel << endl;
}
