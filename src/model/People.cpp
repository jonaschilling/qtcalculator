/*
 * People.cpp
 *
 *  Created on: Jun 28, 2016
 *      Author: jschilling
 */

#include "People.h"

#include <iostream>

using namespace std;

People::People(string name, unsigned int age) {
	this->mName = name;
	this->mAge  = age;
}

People::~People() {
	cout << this->getName() << " is destroyed!" << endl;
}

void People::scream(){
	cout << "I am a MAMMAL and People " << mName << mAge << endl;
}
