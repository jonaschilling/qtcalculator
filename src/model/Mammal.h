/*
 * Mammal.h
 *
 *  Created on: Jun 28, 2016
 *      Author: jschilling
 */

#ifndef MAMMAL_H_
#define MAMMAL_H_

class Mammal {
public:
	Mammal(){};
	virtual ~Mammal(){};

	virtual void scream() = 0;
};

#endif /* MAMMAL_H_ */
