/*
 * Student.h
 *
 *  Created on: Jun 28, 2016
 *      Author: jschilling
 */

#ifndef STUDENT_H_
#define STUDENT_H_

#include "People.h"

#include <string.h>

class Student : public People {

public:
	Student(int matrikel, std::string name, int age);
	virtual ~Student();

	virtual void scream();

private:
	int mMatrikel;


public:
	int getMatrikel() const {
		return mMatrikel;
	}

	void setMatrikel(int matrikel) {
		mMatrikel = matrikel;
	}
};

#endif /* STUDENT_H_ */
