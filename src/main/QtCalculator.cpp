//============================================================================
// Name        : QtCalculator.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <memory>

#include <stdio.h>

#include <exception>

#include "../model/People.h"
#include "../model/Student.h"
#include "../model/Mammal.h"
#include "../model/Player.h"

#include "../data/datastructs.h"

using namespace std;


void initArray(int& fisch);
void manipulateStruct(johnson::data& jDs);


int main() {
	auto_ptr<Student> apPeople (new Student(654654,"Hans Franz",58));

	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!

	People* peoples = new People("Jonas Schilling",26);

	string newName = "Johnny";

	string* pNewName = &newName;

	peoples->setName(newName);

	((People) *peoples).getAge();


	Student* johnny = new Student(51124,"Johansson",26);


	cout << peoples->getName() << ((Student) *johnny).getMatrikel() << endl;

	string testString = peoples->getName();

	testString += "Fisch";

	cout << testString << endl;

	cout << apPeople->getName() << endl;

	int fisch = 10;

	initArray(fisch);

//	unique_ptr<Mammal> pMammal(&johnny);

    Mammal* pMammal = johnny;

    pMammal->scream();

    johnny->scream();

    johnson::data ds = {};

    johnson::data* pDs = (johnson::data*) calloc(1, sizeof(johnson::data));

    pDs->next = &ds;

    pDs->number = 100;

    cout << pDs->number << endl;

    manipulateStruct(*pDs);

    cout << pDs->number << endl;

    try {

		Player player = Player(10);

		player.setErg(5,1,3);

		cout << player.getErg(5)[0] << ":" << player.getErg(5)[1] << endl;

    } catch (exception& e) {

    	cout << endl << endl << endl << endl;
	}


	return 0;
}

void initArray(int& fisch){
	fisch = 11;
}

void manipulateStruct(johnson::data& jDs){
	jDs.number = 11111;
}



